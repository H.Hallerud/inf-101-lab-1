package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Implement the methods task1, and task2.
 * These programming tasks was part of lab1 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/1/
 */
public class Lab1 {

    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        task1();
        task2();
    }

    public static void task1() {
        String message = "Hei, det er meg, datamaskinen.\nHyggelig å se deg her.\nLykke til med INF101!";
        System.out.println(message);
    }

    public static void task2() {
        sc = new Scanner(System.in); // Do not remove this line

        String answerAdresse = "";
        List<String> queryList = new ArrayList<>();

        String name = readInput("Hva er ditt navn? ");
        queryList.add(name);
        String adresse = readInput("Hva er din adresse? ");
        queryList.add(adresse);
        String poststed = readInput("Hva er ditt postnummer og poststed? ");
        queryList.add(poststed);

        for (int i = 0; i < 3; i++){
            answerAdresse += queryList.get(i) + "\n";
        }
        
        System.out.println(name + "s adresse er:" + "\n");
        System.out.println(answerAdresse.trim());
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */

    public static String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        //System.out.println(userInput);
        return userInput;
    }


}
