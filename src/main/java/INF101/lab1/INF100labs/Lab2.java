package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;



/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Nue", "Action", "Champion");
        //isLeapYear(1996);
        //isEvenPositiveInt(0);
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        List<String> words = new ArrayList<>();
        words.add(word1);
        words.add(word2);
        words.add(word3);

        ArrayList<String>wordsFinal = new ArrayList<>();

        int longestWord = (Collections.max(words, Comparator.comparing(String::length))).length();

        for(int i = 0; i < words.size(); i++){
            if (words.get(i).length() == longestWord){
                wordsFinal.add(words.get(i));
            }
        }
        wordsFinal.forEach(i ->System.out.println(i));
    }

    public static boolean isLeapYear(int year) {
        if ((year % 100) == 0){
            if ((year % 400) == 0){
                return true;
            }
            else{
                return false;
            }
        }
        else if ((year % 4) == 0){
            return true;
        }
        else{
            return false;
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num<0){
            return false;
        }
        else if ((num % 2) == 0){
            return true;
        }
        else{
            return false;
        }
    }
}
