package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(43);
        multiplicationTable(6);
        crossSum(515);
    }

    public static void multiplesOfSevenUpTo(int n) {
        // Fin amount of multiples. The fractions should get truncated and we are left with a whole number
        int amountOfMultiples = n / 7;

        for (int i = 1; i <= amountOfMultiples; i++){
            System.out.println(7*i);
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++){
            System.out.print(i);
            System.out.print(": ");
            for (int j = 1; j <= n; j++){
                System.out.printf("%d ",j*i);
            }
            System.out.println("");
        }
    }

    public static int crossSum(int num) {
        int sum = 0;
        String numToString = Integer.toString(num);

        for (int i = 0; i < numToString.length(); i++){
            char enkeltTall = numToString.charAt(i);
            int nyttTall = Character.getNumericValue(enkeltTall);
            sum += nyttTall;
        }

        System.out.println(sum);
        return sum;
    }

}