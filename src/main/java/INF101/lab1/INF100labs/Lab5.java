package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> List1 = new ArrayList<>(Arrays.asList(1,2,3,4,5,6));
        //multipliedWithTwo(List1);
        //removeThrees(List1);
        //niqueValues(List1);
        addList(List1, List1);
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> ansList = new ArrayList<>();
        for(int i = 0; i < list.size(); i++){
            int tall = list.get(i);
            ansList.add(tall*2);
        }
        System.out.println(ansList);
        return ansList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> ans2List = new ArrayList<>();
        for(int i = 0; i < list.size();i++){
            int tall = list.get(i);
            if (tall == 3){}

            else{
                ans2List.add(tall);
            }
        }
        System.out.println(ans2List);
        return ans2List;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> ans3List = new ArrayList<>();
        for(int i = 0; i < list.size(); i++){
            int verdi = list.get(i);
            if (ans3List.contains(verdi)){}

            else{
                ans3List.add(verdi);
            }
        }
        System.out.println(ans3List);
        return ans3List;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++){
            int tallA = a.get(i);
            int tallB = b.get(i);
            int nyttTall = tallA + tallB;
            a.set(i, nyttTall);
        }
        System.out.println(a);
    }
}