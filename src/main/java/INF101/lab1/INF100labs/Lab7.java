package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> kulListe = new ArrayList<>();
        kulListe.add(new ArrayList<>(Arrays.asList(1, 3, 4)));
        kulListe.add(new ArrayList<>(Arrays.asList(2, 8, 6)));
        kulListe.add(new ArrayList<>(Arrays.asList(2, 3, 3)));
        //kulListe.add(new ArrayList<>(Arrays.asList(2, 3, 3)));
        
        //removeRow(kulListe, 0);
        allRowsAndColsAreEqualSum(kulListe);
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
        for(int i = 0; i < grid.size(); i++){
            System.out.println(grid.get(i));
        }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        //int verdi = grid.get(0).get(1); 
        for(int i = 0; i < grid.size(); i++){
            int sumRow = 0; 
            ArrayList<Integer> row = grid.get(i);
            
            for(int j = 0; j < row.size(); j++){
                sumRow += row.get(j);
            }

            int sumCol = 0;

            for(int a = 0; a < grid.size(); a++){
                sumCol += grid.get(a).get(i);
            }
            System.out.println(row);
            System.out.println(sumRow);
            System.out.println(sumCol);
            if (sumRow != sumCol){
                return false;
            }
        }
        return true;
        }

}